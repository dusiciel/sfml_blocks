#include "Board.hpp"
#include "Screen.hpp"
#include "Game.hpp"

Game::Game(int timeToDrop, unsigned int maximumLines):
timeToDrop(timeToDrop),
maximumLines(maximumLines){
}

void Game::run(Screen& screen, Board& board){
    while (screen.isOpen())
    {
        screen.clear();
        sf::Event event;

        screen.drawHorizontalLine(maximumLines);

        while (screen.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                screen.close();
            if (event.type == sf::Event::MouseButtonPressed){
                board.addSquareBelow(event.mouseButton.x/screen.getBlockSide());
            }
        }
        if (clock.getElapsedTime().asMilliseconds() >= timeToDrop){
            board.addRowAtTheTop();
            clock.restart();
        }
        board.draw(screen);

        screen.display();
        if (board.getSize() > maximumLines){
            screen.close();
        }
    }
}
