#pragma once

#include <vector>
#include <memory>
#include <map>

#include "Square.hpp"
#include "SquaresBuilder.hpp"

struct Screen;

struct Row{
    Row(int numberOfColumns, const std::vector<int>& squaresColumns, SquaresBuilder& squaresBuilder);
    void addSquareAt(unsigned int column);
    void draw(Screen& screen, int height) const;
    bool squareExist(unsigned int column);
    bool isFull();
    unsigned int countSquares();
    bool isMergeable(const Row& row);
    void merge(Row& row);
    void clear();
private:
    void createSquare(unsigned int column);

    SquaresBuilder& squaresBuilder;
    std::vector<std::unique_ptr<Square>> squares;
};
