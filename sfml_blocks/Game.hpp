#pragma once

#include <SFML/System/Clock.hpp>

struct Board;
struct Screen;

struct Game{
    Game(sf::Int32 timeToDrop, unsigned int maximumLines);
    void run(Screen& screen, Board& board);

private:
    int timeToDrop;
    unsigned int maximumLines;
    sf::Clock clock;
};
