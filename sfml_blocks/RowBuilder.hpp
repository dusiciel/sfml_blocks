#pragma once

#include "Row.hpp"

std::vector<int> getRandomSquareIndexes(int numOfSquares, int min, int max);

struct RowBuilder{
    RowBuilder(size_t numberOfColumns, SquaresBuilder& squaresBuilder);
    Row build();
    Row build(const std::vector<int>& squaresColumns);
private:
    size_t numberOfColumns;
    SquaresBuilder& squaresBuilder;
};
