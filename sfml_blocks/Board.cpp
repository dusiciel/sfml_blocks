#include "Row.hpp"
#include "Board.hpp"

struct Screen;

Board::Board(size_t numOfRows, size_t fillRows, RowBuilder& rb):
    numOfRows(numOfRows),
    rowBuilder(rb)
{
    for(size_t i = 0; i < numOfRows; i++){
        if (i < fillRows)
            rows.push_back(rowBuilder.build());
        else
            rows.push_back(rowBuilder.build({}));
    }
}

void Board::addSquareBelow(int column){
    Row tmp = rowBuilder.build({column});
    if(!rows.rbegin()->isMergeable(tmp)){
        return;
    }

    for(auto row = rows.rbegin(); row != rows.rend(); row = std::next(row)){
        auto next = std::next(row);
        if(!next->isMergeable(tmp) or next == rows.rend()){
            row->merge(tmp);
            if(row->isFull()) row->clear();
            break;
        }
    }
}

void Board::addRowAtTheTop(){
    //rows.emplace_back(numOfColumns, getRandomSquareIndexes(numOfColumns, 1, numOfColumns-1), squaresBuilder);
}

void Board::draw(Screen& screen) {
    for(size_t i = 0; i < numOfRows; i++)
        rows[i].draw(screen, i);
}

size_t Board::getSize(){
    size_t result = 0;
    for (auto& row: rows)
        result += (row.countSquares() < 1)?0:1;
    return result;
}
