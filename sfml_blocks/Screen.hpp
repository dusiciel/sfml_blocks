#pragma once

#include <SFML/Graphics.hpp>

constexpr int RESOLUTION_WIDTH = 540;
constexpr int RESOLUTION_HEIGHT = 860;

struct Screen{
    Screen(unsigned int columns);
    bool isOpen();
    void clear();
    void display();
    void close();
    void drawHorizontalLine(int height);
    bool pollEvent(sf::Event& event);
    float getBlockSide();
    void draw(const sf::Drawable& drawable, const sf::RenderStates& states = sf::RenderStates::Default);
private:
    sf::RenderWindow window;
    const float blockSide;
};
