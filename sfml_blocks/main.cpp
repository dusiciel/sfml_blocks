#include "Board.hpp"
#include "Game.hpp"
#include "Screen.hpp"
#include "SquaresBuilder.hpp"
#include "RowBuilder.hpp"

namespace{
    constexpr unsigned int ROWS = 20;
    constexpr unsigned int MAX_ROWS = 30;
    constexpr unsigned int COLUMNS = 20;
}

int main()
{
    srand(time(NULL));

    Screen screen(COLUMNS);
    SquaresBuilder squaresBuilder(screen.getBlockSide());
    RowBuilder rowBuilder(COLUMNS, squaresBuilder);
    Board board(MAX_ROWS, ROWS, rowBuilder);

    Game(5000, MAX_ROWS).run(screen, board);
    return 0;
}
