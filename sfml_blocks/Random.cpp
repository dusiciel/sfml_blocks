#include "Random.hpp"

std::random_device Random::rd;
std::mt19937 Random::mt(Random::rd());

int Random::getBetween(int from, int to){
    std::uniform_int_distribution<int> dist(from, to);
    return dist(mt);
}


