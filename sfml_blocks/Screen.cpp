#include "Screen.hpp"

Screen::Screen(unsigned int columns):
    window(sf::VideoMode(RESOLUTION_WIDTH, RESOLUTION_HEIGHT), "sfml blocks!"),
    blockSide(RESOLUTION_WIDTH / columns)
{}

bool Screen::isOpen(){
    return window.isOpen();
}

void Screen::clear(){
    window.clear();
}

void Screen::display(){
    window.display();
}

void Screen::close(){
    window.close();
}

void Screen::drawHorizontalLine(int height){
    sf::Vertex line[] = {sf::Vertex(sf::Vector2f(0,height*blockSide)), sf::Vertex(sf::Vector2f(RESOLUTION_WIDTH, height*blockSide))};
    window.draw(line, 2, sf::Lines);
}

bool Screen::pollEvent(sf::Event& event){
    return window.pollEvent(event);
}

float Screen::getBlockSide(){
    return blockSide;
}

void Screen::draw(const sf::Drawable& drawable, const sf::RenderStates& states){
    window.draw(drawable, states);
}

