#include <iostream>
#include <map>
#include <cmath>
#include <numeric>
#include <algorithm>

#include "Random.hpp"
#include "Row.hpp"
#include "Square.hpp"
#include "SquaresBuilder.hpp"

struct Screen;

Row::Row(int numberOfColumns, const std::vector<int>& squaresColumns, SquaresBuilder& squaresBuilder) :
    squaresBuilder(squaresBuilder)
{
    squares.resize(numberOfColumns);
    for(auto column: squaresColumns)
        createSquare(column);
}

void Row::addSquareAt(unsigned int column){
    createSquare(column);
}

void Row::draw(Screen& screen, int height) const{
    for (const auto& square: squares){
        if(square) square->draw(screen, height);
    }
}

bool Row::squareExist(unsigned int column){
    return (squares[column])?true:false;
}

bool Row::isFull(){
    return countSquares() >= squares.size();
}

unsigned int Row::countSquares(){
    unsigned int result = 0;
    for (const auto& square: squares)
        if(square) result++;
    return result;
}

bool Row::isMergeable(const Row& row){
    for(size_t i = 0; i < squares.size(); i++)
        if(row.squares[i] && squares[i])
            return false;
    return true;
}

void Row::merge(Row& row){
    for(size_t i = 0; i < squares.size(); i++)
        if(row.squares[i]) squares[i] = std::move(row.squares[i]);
}

void Row::clear(){
    for(size_t i = 0; i < squares.size(); i++)
        squares[i] = nullptr;
}

void Row::createSquare(unsigned int column){
    if (!isFull() && !squareExist(column)){
        squares[column] = make_unique<Square>(squaresBuilder.build(column));
    }
}
