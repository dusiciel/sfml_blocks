#include "SquaresBuilder.hpp"

sf::Color randomColor(){
    return sf::Color(Random::getBetween(63,255),Random::getBetween(63,255),Random::getBetween(63,255));
}

    SquaresBuilder::SquaresBuilder(float side):
        side(side)
    {}

    Square SquaresBuilder::build(int x) const{
        return Square(side, x, randomColor());
    }
